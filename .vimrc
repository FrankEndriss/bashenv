colo desert
syntax on
set softtabstop=4 shiftwidth=4 expandtab

" block cursor and colors as of
" https://superuser.com/questions/634326/how-can-i-get-a-block-cursor-in-vim-in-the-cygwin-terminal
let &t_ti.="\e[1 q"
let &t_SI.="\e[5 q"
let &t_EI.="\e[1 q"
let &t_te.="\e[0 q"

" see https://stackoverflow.com/questions/2355834/how-can-i-autoformat-indent-c-code-in-vim
autocmd BufNewFile,BufRead *.cc set formatprg=astyle\ -A14\ --lineend=linux\ --break-one-line-headers\ --max-code-length=160

nnoremap <F7> :w \| !cf test<CR>
nnoremap <F9> :w \| !cf submit<CR>

" Specify a directory for plugins
" " - For Neovim: stdpath('data') . '/plugged'
" " - Avoid using standard Vim directory names like 'plugin'
" see https://github.com/junegunn/vim-plug
call plug#begin('~/.vim/plugged')

Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'jakobkogler/Algorithm-DataStructures'

call plug#end()

nmap <leader>alg :AlgDS<CR>
nmap <F6> :AlgDS<CR>
